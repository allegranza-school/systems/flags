<?php
    $flagsdata = NULL;
    $flagsnum = NULL;

    function loadData($path)
    {
        global $flagsdata, $flagsnum;
        $rawdata = file_get_contents($path);
        $flagsdata = json_decode($rawdata);
        $flagsnum = count($flagsdata);
    };

    function getFlagColors($id)
    {
        global $flagsdata;
        return $flagsdata[$id]->colors;
    };

    function getFlagName($id)
    {
        global $flagsdata;
        return $flagsdata[$id]->name;
    };

    function genFlagElements($flagcolors){
        $html = "";
        foreach($flagcolors as $color){
            $html .= "<div class='flag-elem' style='background-color: $color;'>&nbsp;</div>";
        };
        return $html;
    }
?>

<html>

<head>
    <title>flags exercise</title>
    <?php include "./style/bootstrap.php" ?>
    <link rel="stylesheet" href="./style/style.css">
</head>

<body>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Flags</h1>
            <p class="lead">Add flags data into <kbd>data.json</kbd> file to visualize it</p>
        </div>
    </div>


    <div class="flags-container">

        <?php
        $flag_template = '
            <div class=\'card flag-card\'>
                <div class=\'card-header\'>
                    $name
                </div>
                <div class=\'card-body\'>
                    <div class=\'flag\'>
                        $flag_elements
                    </div>
                </div>
            </div>
        ';
        
        loadData("./data.json");
        
        for ($id = 0; $id < $flagsnum; $id++) {
            $flag_name = getFlagName($id);
            $flag_colors = getFlagColors($id);
            $flag_elements = genFlagElements($flag_colors);
            $flag_vars = array('$name' => $flag_name, '$flag_elements' => $flag_elements);
            echo strtr($flag_template, $flag_vars);
        }

        
        ?>
    </div>

</body>
</html>