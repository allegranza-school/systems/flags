# Exercise: flags
_Date: 9/21/2020_

### Description
Creare una pagina PHP che visualizzi le bandiere di alcune nazioni.
Il nome delle nazioni e i colori che compongono la bandiera sono memorizzati in un file con una struttura specifica (a scelta)

Formato Prof: `id**name**color1**color2**color3`

Esempio Prof: 
``` 
    0**Italia**00FF00**FFFFFF**FF0000
    1**Polonia**FFFFFF**FF0000
    2**Austria**FF0000**FFFFFF**FF0000
```

*Nota: le bandiere possono avere un numero di colori variabile*

### Additional
Creare la bandiera dei pirati usando la tecnica che piu' si preferisce.
